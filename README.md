# Туториал по SwiftUI
Этот проект сделан по обучающим материалам от Apple по SwiftUI

## Chapter 1

### Creating and combining views
<img src="Screens/Chapter1/Less1/Screen1.png" width="300" height="630"/>

### Building lists and navigation
<img src="Screens/Chapter1/Less2/Screen1.png" width="300" height="630"/>
<img src="Screens/Chapter1/Less2/Screen2.png" width="300" height="630"/>

### Handling user input
<img src="Screens/Chapter1/Less3/SwiftUIDemoChapter1Less3.gif" width="300" height="630"/>

## Chapter 2 

### Drawing and animation
<img src="Screens/Chapter2/SwiftUIDemoChapter2Less1.gif" width="300" height="630"/>

## Chapter 3

### Composing complex interfaces
<img src="Screens/Chapter3/Less1/SwiftUIDemoChapter3Less1.gif" width="300" height="630"/>

### Working with UI controls
<img src="Screens/Chapter3/Less2/SwiftUIDemoChapter3Less2.gif" width="300" height="630"/>

## Chapter 4 

### Interfacing with UIKit
<img src="Screens/Chapter4/Less1/SwiftUIDemoChapter4Less1.gif" width="300" height="630"/>

