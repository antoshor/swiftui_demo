//
//  SwiftUI_DemoApp.swift
//  SwiftUI_Demo
//
//  Created by Mac Admin on 27.12.2023.
//

import SwiftUI

@main
struct LandmarksApp: App {
    
    @State private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
         ContentView()
                .environment(modelData)
        }
    }
}

