//
//  CircleImage.swift
//  SwiftUI_Demo
//
//  Created by Mac Admin on 28.12.2023.
//

import SwiftUI

struct CircleImage: View {
    
    var image: Image
    var body: some View {
        
        image
            .resizable()
            .frame(width: 200, height: 200)
        
            .clipShape(Circle())
        
            .overlay(Circle().stroke(.gray, lineWidth: 4))
            .shadow(color: .green, radius: 4)
        
    }
}

#Preview {
    CircleImage(image: ModelData().landmarks[0].image)
}
