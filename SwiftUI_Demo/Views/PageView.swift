//
//  PageView.swift
//  SwiftUI_Demo
//
//  Created by Krasilnikov Anton on 09.01.2024.
//

import SwiftUI

struct PageView<Page: View>: View {
    var pages: [Page]
    @State private var currentPage = 0


    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            PageViewController(pages: pages, currentPage: $currentPage)
            PageControl(numberOfPages: pages.count, currentPage: $currentPage)
                .frame(width: CGFloat(pages.count * 18))
                .padding(.trailing)
        }
        .aspectRatio(3 / 2, contentMode: .fit)
    }
}


#Preview {
    PageView(pages: ModelData().features.map { FeatureCard(landmark: $0) })
}
//        Button {
//            if currentPage < 2 {
//                currentPage += 1
//            } else {
//                currentPage = 0
//            }
//        } label: {
//            Label("Button", systemImage: "star")
//        }
